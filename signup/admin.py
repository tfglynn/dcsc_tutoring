from django.contrib import admin
from .models import Tutee

class TuteeAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, {'fields': ['first_name', 'last_name', 'computer']}),
    ]

admin.site.register(Tutee, TuteeAdmin)
