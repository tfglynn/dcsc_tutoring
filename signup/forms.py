from django import forms
from tutors.models import Course

class SignUpForm(forms.Form):
    first_name = forms.CharField(label='First name', max_length=64)
    last_name = forms.CharField(label='Last name', max_length=64)
    computer = forms.IntegerField(
        label='Computer #',
        min_value=1,
        max_value=70,
    )
    choices = Course.objects.all()
    choices = [(course.short_name, course) for course in choices]
    course = forms.ChoiceField(
        choices=choices,
        widget=forms.RadioSelect,
    )
