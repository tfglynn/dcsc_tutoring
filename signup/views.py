from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.utils import timezone
from tutors.models import Course
from .forms import SignUpForm
from .models import Tutee

def index(request, error=None):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            try:
                course = request.POST['course']
                course = Course.objects.get(short_name=course)
                now = timezone.now()
                tutee = Tutee(
                    first_name=request.POST['first_name'],
                    last_name=request.POST['last_name'],
                    computer=request.POST['computer'],
                    course=course,
                    date_added=now,
                )
                tutee.save()
            except Course.DoesNotExist:
                error = 'Your selected course does not exist.'
                return index(request, error)
            return HttpResponseRedirect('/signup/thanks')
    else:
        form = SignUpForm()
    context = {'form': form, 'error': error}
    return render(request, 'signup/index.html', context)

def thanks(request):
    return render(request, 'signup/thanks.html', {})
