from django.db import models
from tutors.models import Course

class Tutee(models.Model):
    first_name = models.CharField(max_length=64)
    last_name = models.CharField(max_length=64)
    computer = models.PositiveSmallIntegerField()
    course = models.ForeignKey(Course)
    date_added = models.DateTimeField()

    def __str__(self):
        return '{}, {}'.format(self.last_name, self.first_name)
