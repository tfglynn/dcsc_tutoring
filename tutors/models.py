from django.db import models

class Course(models.Model):
    short_name = models.CharField(max_length=16)
    long_name = models.CharField(max_length=64)

    def __str__(self):
        return self.short_name

    def tutors(self):
        """
        Return a list of tutors who teach this course
        """
        return self.tutor_set.all()

class Tutor(models.Model):
    first_name = models.CharField(max_length=64)
    last_name = models.CharField(max_length=64)
    year = models.IntegerField()
    course_set = models.ManyToManyField(Course)

    def __str__(self):
        return ', '.join([self.last_name, self.first_name])

    def courses(self):
        """
        Return a list of courses this tutor teaches
        """
        return self.course_set.all()

class Event(models.Model):
    """
    A period of time when a particular tutor teaches a particular course
    """
    when = models.DateTimeField()
    duration = models.DurationField()
    tutor = models.ForeignKey(Tutor)
    course = models.ForeignKey(Course)

    def __str__(self):
        when = self.when.strftime('%Y-%m-%d %I:%M %p')
        course = self.course.short_name
        tutor = self.tutor.last_name
        return '{}, {}: {}'.format(course, tutor, when)

    def date(self):
        datetime = self.when
        return datetime.date()

    def start(self):
        return self.when

    def start_time(self):
        start = self.start()
        return start.time()

    def end(self):
        start = self.start()
        end = start + self.duration
        return end

    def end_time(self):
        end = self.end()
        return end.time()
