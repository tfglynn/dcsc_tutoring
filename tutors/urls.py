from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.IndexView.as_view(), name='index'),
    url(r'^teaching/([A-Z]+[0-9]+)/$',
        views.TeachingView.as_view(),
        name='teaching'),
]
