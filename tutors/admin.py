from django.contrib import admin
from .models import Tutor, Course, Event

class TutorAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, {'fields': ['first_name', 'last_name', 'year']}),
    ]

class CourseAdmin(admin.TabularInline):
    model = Course
    extra = 1

class EventAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, {'fields': ['course', 'tutor']}),
        ('When?', {'fields': ['when', 'duration']})
    ]
    list_display = ('date', 'course', 'tutor', 'start_time', 'end_time')

admin.site.register(Tutor, TutorAdmin)
admin.site.register(Course)
admin.site.register(Event, EventAdmin)
