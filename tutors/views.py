from django.shortcuts import get_object_or_404
from django.views import generic
from .models import Tutor, Course

class IndexView(generic.ListView):
    template_name = 'tutors/index.html'
    context_object_name = 'tutors_list'
    queryset = Tutor.objects.order_by('last_name')

class TeachingView(generic.ListView):
    """
    Render a list of tutors teaching the requested course
    """
    template_name = 'tutors/teaching.html'
    context_object_name = 'tutors_list'

    def get_queryset(self):
        course = get_object_or_404(Course, short_name=self.args[0])
        self.course = course
        return course.tutors()

    def get_context_data(self, **kwargs):
        context = super(TeachingView, self).get_context_data(**kwargs)
        context['course'] = self.course
        return context
