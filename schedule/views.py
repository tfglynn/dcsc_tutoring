from django.shortcuts import render
from django.utils import timezone
from datetime import timedelta

from tutors.models import Course, Event
from .forms import FindTimesForm

weekdays = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']

def index(request):
    if request.method == 'POST':
        form = FindTimesForm(request.POST)
        if form.is_valid():
            courses = None
            course_names = request.POST.get('courses', None)
            if course_names:
                courses = []
                for name in course_names:
                    try:
                        course = Course.objects.get(short_name=name)
                    except Course.DoesNotExist:
                        continue
                    courses.append(course)
            days = request.POST.get('days', None)
            return render_criteria(request, form, courses, days)
    else:
        form = FindTimesForm()
    return render_this_week(request, form)

def render_this_week(request, form):
    """
    Render a schedule of all events this week.
    """
    today = timezone.now()
    today = today.replace(hour=0, minute=0, second=0, microsecond=0)
    this_week_start = today - timedelta(days=(today.weekday() + 1))
    next_week_start = this_week_start + timedelta(weeks=1)
    this_week_end = next_week_start - timedelta(days=1)

    events = Event.objects.filter(
        when__gte=this_week_start
    ).filter(
        when__lt=next_week_start
    )

    weekday_events = []
    for number, name in enumerate(weekdays):
        this_day = this_week_start + timedelta(days=number)
        events_this_day = filter(
            lambda event: event.date() == this_day.date(),
            events,
        )
        weekday_events.append((name, list(events_this_day)))

    start_date = this_week_start.date()
    end_date = this_week_end.date()
    context = {
        'weekday_events': weekday_events,
        'start_date': start_date,
        'end_date': end_date,
        'form': form,
    }

    return render(request, 'schedule/index.html', context)

def render_criteria(
    request,
    form,
    courses=None,
    days=None,
    start=None,
    end=None,
):
    # This function should render the results page.
    # Right now this is a placeholder.
    return render_this_week(request, form)
