from django import forms
from tutors.models import Course

class FindTimesForm(forms.Form):
    course_choices = Course.objects.all()
    course_choices = [(course.short_name, course) for course in course_choices]
    courses = forms.MultipleChoiceField(
        choices=course_choices,
        widget=forms.CheckboxSelectMultiple,
        required=False,
    )

    day_choices = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']
    day_choices = [(day, day) for day in day_choices]
    days = forms.MultipleChoiceField(
        choices=day_choices,
        widget=forms.CheckboxSelectMultiple,
        required=False,
    )
